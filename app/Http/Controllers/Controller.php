<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  public function index($id = null){
    if(!Schema::hasTable('haber')){
      return "Yapım Aşamasındayız!";
    }

    $count = DB::table('haber')->count();
    if(is_null($id)){     // siteye ilk girildiğinde $id null oluyor
       $id = 0;
    }
    $id = (int)$id;
    if(is_int($id)){
      if($id<=$count && $id>=0){  // hatali karşılaştırma, $count tan büyük id değerleri vardır
        // $id = $id * 5;
        $haberler = DB::table('haber')->orderBy('pubdate','desc')->skip($id*5)->limit(5)->get();

        return view('index',compact('haberler','count','id'));
      }
    }else{
      $haberler = DB::table('haber')->orderBy('pubdate','desc')->limit(5)->get();
      return view('index',compact('haberler','count','id'));
    }
  }

  public function paper($id = null){
    if(is_null($id)){     // siteye ilk girildiğinde $id null oluyor
       $id = 0;
    }
    $id = (int)$id;

    $haber = DB::table('haber')->where('id','=',$id)->get();
    return view('singlepage',compact('haber'));

  }
}
