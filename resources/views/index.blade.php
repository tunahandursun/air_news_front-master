@extends('templates.indextemplate')
<!-- btn btn-mini btn-inverse -->
@section('haber')

<?php
  if ($id == 0) {

  $haber = DB::table('haber')->where('picture','<>','None')->limit(10)->get();
 ?>


  <div class="flexslider">
      <ul class="slides">
        <?php for ($i=0; $i < 10; $i++) {  ?>
          <li><a href="<?=URL::to('/').'/'."paper".'/'.$haber[$i]->id?>"><img src="<?=$haber[$i]->picture?>" alt="slider" /></a></li>
        <?php } ?>
      </ul>
    </div>
<?php
  }
 ?>

<br/>
<?php foreach ($haberler as $haber ) { ?>
<article class="clearfix">
    <?php if($haber->picture == 'none') {$haber->picture = 'assets/front/img/gallery/gallery-img-1-4col.jpg';}  ?>
    <a href="<?=URL::to('/').'/'.'paper/'.$haber->id?>"><img src="<?=$haber->picture?>"  alt="" class="align-left"></a>
    <h4 class="title-bg"><a href="<?=URL::to('/').'/'."paper".'/'.$haber->id?>" style="color:#d8450b"> <?=$haber->title?> </a></h4>
        <p>&nbsp&nbsp&nbsp&nbsp<?=$haber->description?> </p>
        <a href="<?=URL::to('/').'/'.'paper/'.$haber->id?>"><button class="btn btn-small" type="button">Devamı</button></a>
        <div class="post-summary-footer">
            <ul class="post-data-3">
                <li><i class="icon-calendar"></i> <?=$haber->pubdate?></li>
            </ul>
        </div>
</article>
<?php } ?>
@endsection

@section('sayfa_numaralari')
    <li><a href='<?=URL::to('/').'/'.($id-1)?>'>Prev</a></li>
    <li><a href='<?=URL::to('/')."/".($id-2)?>'><?=$id-2?></a></li>
    <li><a href='<?=URL::to('/')."/".($id-1)?>'><?=$id-1?></a></li>
    <li class="active"><a href='<?=URL::to('/')."/".$id ?>'><?=$id ?></a></li>
    <li><a href='<?=URL::to('/')."/".($id+1)?>'><?=$id+1?></a></li>
    <li><a href='<?=URL::to('/')."/".($id+2)?>'><?=$id+2?></a></li>
    <li><a href='<?=URL::to('/').'/'.($id+1)?>'>Next</a></li>
@endsection
