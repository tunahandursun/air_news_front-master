@extends('templates.singletemplate')

@section('title')
  <?=$haber[0]->title?>
@endsection
@section('haber')
<article>
    <h3 class="title-bg"><a href="#"><?=$haber[0]->title?></a></h3>
    <div class="post-content">
        <a href="#"><img src="<?=$haber[0]->picture?>"></a>

        <div class="post-body">
          <?=$haber[0]->content?>
        </div>

        <div class="post-summary-footer">
            <ul class="post-data">
                <li><i class="icon-calendar"></i>  <?=$haber[0]->pubdate?></li>
            </ul>
        </div>
    </div>
</article>
@endsection

@section('yorumlar')
  <section class="comments">
      <!-- Comment Form -->
      <div id="disqus_thread"></div>
      <script>

      /**
      *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
      *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
      /*
      var disqus_config = function () {
      this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
      this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
      };
      */
      (function() { // DON'T EDIT BELOW THIS LINE
      var d = document, s = d.createElement('script');
      s.src = 'https://havahaber-com.disqus.com/embed.js';
      s.setAttribute('data-timestamp', +new Date());
      (d.head || d.body).appendChild(s);
      })();
      </script>
      <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

  </section>

@endsection
