@include('templates.headertemplate')

<body>
	<div class="color-bar-1"></div>
    <div class="color-bar-2 color-bg"></div>

    <div class="container main-container">

		@include('templates.navigationtemplate')

    <!-- Blog Content
    ================================================== -->
    <div class="row">

        <!-- Blog Posts
        ================================================== -->
        <div class="span8 blog">

            <!-- Blog Post 1 -->
						@yield('haber')

            <!-- Pagination -->
            <div class="pagination">
                <ul>
										@yield('sayfa_numaralari')
                </ul>
            </div>
        </div>

						@include('templates.sidebartemplate')

    </div>

    </div> <!-- End Container -->

		@include('templates.footertemplate')
