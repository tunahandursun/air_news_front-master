<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>@yield('title',"Havacılık Haberleri")</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS
================================================== -->
<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?=url('/')?>/assets/front/css/bootstrap.css">
<link rel="stylesheet" href="<?=url('/')?>/assets/front/css/bootstrap-responsive.css">
<link rel="stylesheet" href="<?=url('/')?>/assets/front/css/jquery.lightbox-0.5.css">
<link rel="stylesheet" href="<?=url('/')?>/assets/front/css/custom-styles.css">
<link rel="stylesheet" href="<?=url('/')?>/assets/front/css/flexslider.css" />
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link rel="stylesheet" href="css/style-ie.css"/>
<![endif]-->

<!-- Favicons
================================================== -->
<link rel="shortcut icon" href="<?=url('/')?>/assets/front/img/favicon.ico">
<link rel="apple-touch-icon" href="<?=url('/')?>/assets/front/img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?=url('/')?>/assets/front/img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?=url('/')?>/assets/front/img/apple-touch-icon-114x114.png">

<!-- JS
================================================== -->
<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
<script src="<?=url('/')?>/assets/front/js/bootstrap.js"></script>
<script src="<?=url('/')?>/assets/front/js/jquery.custom.js"></script>
<script src="<?=url('/')?>/assets/front/js/jquery.flexslider.js"></script>
<script src="<?=url('/')?>/assets/front/js/jquery.custom.js"></script>
<script type="text/javascript">
  $(document).ready(function () {

      $("#btn-blog-next").click(function () {
        $('#blogCarousel').carousel('next')
      });
       $("#btn-blog-prev").click(function () {
        $('#blogCarousel').carousel('prev')
      });

       $("#btn-client-next").click(function () {
        $('#clientCarousel').carousel('next')
      });
       $("#btn-client-prev").click(function () {
        $('#clientCarousel').carousel('prev')
      });

  });

   $(window).load(function(){

      $('.flexslider').flexslider({
          animation: "slide",
          slideshow: true,
          start: function(slider){
            $('body').removeClass('loading');
          }
      });
  });
</script>
</head>
