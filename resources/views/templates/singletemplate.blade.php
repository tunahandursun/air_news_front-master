@include('templates.headertemplate')

<body>
	<div class="color-bar-1"></div>
    <div class="color-bar-2 color-bg"></div>

    <div class="container main-container">

    @include('templates.navigationtemplate')

    <!-- Blog Content
    ================================================== -->
    <div class="row"><!--Container row-->

        <!-- Blog Full Post
        ================================================== -->
        <div class="span8 blog">

            <!-- Blog Post 1 -->
						@yield('haber')

        <!-- Post Comments
        ================================================== -->
						@yield('yorumlar')

				<!-- Close comments section-->
        </div><!--Close container row-->

      @include('templates.sidebartemplate')

    </div>

    </div> <!-- End Container -->

@include('templates.footertemplate')
