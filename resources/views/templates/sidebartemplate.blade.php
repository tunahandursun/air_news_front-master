
        <!-- Blog Sidebar
        ================================================== -->
        <div class="span4 sidebar">


            <!--Popular Posts-->
            <h5 class="title-bg">Popüler Gönderiler</h5>
            <ul class="popular-posts">
              <?php
              $haber = DB::table('haber')->inRandomOrder()->where('picture','<>','None')->limit(3)->get();

                for ($i=0; $i < count($haber); $i++) {

                  ?>

                <li>
                    <a href="/paper/<?=$haber[$i]->id?>"><img src="<?=$haber[$i]->picture?>" alt="Popular Post"></a>
                    <h6><a href="/paper/<?=$haber[$i]->id?>"><?=$haber[$i]->title?></a></h6>
                    <em><?=$haber[$i]->pubdate?></em>
                </li>
                <?php } ?>

            </ul>

            <!--Video Widget
            <h5 class="title-bg">Video Widget</h5>
            <iframe src="http://player.vimeo.com/video/24496773" width="370" height="208"></iframe> -->
        </div>
